import React from 'react'
import '../css/Bootstrap.css';
import '../css/login.css';

import { sessionToken } from "../utilidades/UseSession";
import swal from "sweetalert";
import { InicioSesion } from "../hooks/ConexionSw";
import { useForm } from "react-hook-form";
import { useNavigate, Link } from 'react-router-dom';
export const Login = () => {

  const navegacion = useNavigate();
  const msgError = (texto) => swal({
    title: 'Error',
    text: texto,
    icon: 'error',
    button: 'Aceptar',
    timer: 2000

  });
  console.log('data session');
  const { register, handleSubmit, formState: { errors } } = useForm();
  
  const onSubmit = (data) => {
    var datos = {
      'correo': data.correo,
      'clave': data.clave
    };
    InicioSesion(datos).then((info) => {
      if (info && info.data.token) {
        sessionToken(info.data.token);
        navegacion('/InicioG');
      } else {
        if (info && info.msg) {//cuado sale un error de trasaccion
          msgError(info.msg);
        } else {
          //cuando sale erro d evalidacion en backend
          msgError('Error de validacion');
        }

      }
    }, (error) => {
      msgError(error.message);
      console.log(error);

    });
  }

  return (
    <div >
      <nav className="navbar navbar-expand-lg bg-dark navbar-dark">
                <div className="container">
                    <a className="nav-link " href="/">NETMEDIA</a>


                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                            <Link to="/Iniciar_Sesion" className='nav-link' style={{ color: 'cyan' }} >Iniciar sesion</Link>
                        </div>
                    </div>
                </div>
            </nav>
      <div className="container">
        <div className="d-flex justify-content-center h-100">
          <div className="card center">
            <div className="card-header">
              <h3>Inicio de Sesion</h3>
              <div className="d-flex justify-content-end social_icon">
              </div>
            </div>
            <div className="card-body">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text"><i className="fas fa-user"></i></span>
                  </div>
                  <input {...register('correo', { required: true, pattern: /^\S+@\S+$/i })} type="text" className="form-control" placeholder="Usuario" />
                  {errors.correo && errors.correo.type === 'required' && <div className="alert alert-danger">Ingrese el correo</div>}
                  {errors.correo && errors.correo.type === 'pattern' && <div className="alert alert-danger">Ingrese un correo valido</div>}
                </div>
                <div className="input-group form-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text"><i className="fas fa-key"></i></span>
                  </div>
                  <input {...register('clave', { required: true })} type="password" className="form-control" placeholder="Clave" />
                  {errors.clave && errors.clave.type === 'required' && <div className="alert alert-danger">Ingrese el dato</div>}
                </div>
                <div className="form-group">
                  <button type="submit" className="btn center login_btn">Iniciar Sesion</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
