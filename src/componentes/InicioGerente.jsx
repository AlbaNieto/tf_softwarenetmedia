import React from 'react';
import NavBarGerente from './NavBarGerente';
import { Link } from 'react-router-dom';
const InicioGerente = () => {
    return (
        <div>
            <NavBarGerente/>
        <div className="container" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh', }}>
            <div>
                <div style={{ backgroundColor: 'rgb(1, 143, 143)', width: '68rem' }} >
                    <header>
                        <div className="text-center">
                            <img src={require('../img/Saludar.png')} style={{ float: 'left', height: '125px' }} />
                            <h1 style={{ color: 'cyan' }}>Bienvenido Usuario</h1>
                            <h2>Que función deseas realizar hoy</h2>
                            <p>Selecciona una de las opciones ubicadas en la parte inferior para continuar</p>
                        </div>
                    </header>
                </div>
                <div className="card" style={{ width: '13.6rem', backgroundColor: 'rgb(1, 143, 143)', float: 'left' }} >

                    <div className="card-body">
                        <img className="img-fluid img-icon" style={{margin: "10px"}} src={require('../img/AgregarReparacion.png')} />
                        <button className="btn btn-dark" style={{ color: 'cyan' }}>
                        <Link to="/Agendar_Reparacion" className='nav-link' style={{ color: 'cyan' }}>Agendar Reparación</Link>
                        </button>
                    </div>
                </div>
                <div className="card" style={{ width: '13.6rem', backgroundColor: 'rgb(1, 143, 143)', float: 'left' }}>
                    <div className="card-body">
                        <img className="img-fluid img-icon" style={{margin: "10px"}} src={require('../img/GestionarFechas.png')} />
                        <button className="btn btn-dark" style={{ color: 'cyan' }}>
                        <Link to="/Gestion_Fechas" className='nav-link' style={{ color: 'cyan' }}>Gestionar Fechas</Link>
                        </button>
                    </div>
                </div>
                <div className="card" style={{ width: '13.6rem', backgroundColor: 'rgb(1, 143, 143)', float: 'left' }}>
                    <div className="card-body">
                        <img className="img-fluid img-icon" style={{margin: "10px"}} src={require('../img/BuscarReparacion.png')} />
                        <button className="btn btn-dark" style={{ color: 'cyan' }}>
                        <Link to="/Buscar" className='nav-link' style={{ color: 'cyan' }}>Buscar Reparación</Link>
                        </button>
                    </div>
                </div>
                <div className="card" style={{ width: '13.6rem', backgroundColor: 'rgb(1, 143, 143)', float: 'left' }}>
                    <div className="card-body">
                        <img className="img-fluid img-icon" style={{margin: "10px"}} src={require('../img/Facturar.png')} />
                        <button className="btn btn-dark" style={{ color: 'cyan' }}>
                            <Link to="/Facturacion" className='nav-link' style={{ color: 'cyan' }}>Facturar Reparación</Link></button>
                    </div>
                </div>
                
            </div>

        </div>
        </div>
    );
};

export default InicioGerente;