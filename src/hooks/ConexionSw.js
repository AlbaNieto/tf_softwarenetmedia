import { useState, useEffect } from "react";
import axios from "axios";
import { CerrarSession, ObtenerSesion, Sesion, getSession } from "../utilidades/UseSession";

const URL = "http://localhost:8095/api/v1";

export const Opac = (accion = true) => {
  const [info, setInfo] = useState(null);
  const [error, setError] = useState(null);
  useEffect(() => {
    if (accion) callApi();
  }, []);
  const callApi = async (nombre) => {
    try {
      const { data, status, statusText } = await axios.get(URL + "/opac");
      setInfo(data);
      console.log(data);
    } catch (error) {
      //console.log(error);
      setError(error);
    }
  };
  return { info, error, execute: callApi };
};

export const InicioSesion1 = (data, accion = true) => {
  const [info, setInfo] = useState(null);
  const [error, setError] = useState(null);
  useEffect(() => {
    if (accion) callApi(data);
  }, []);
  const callApi = async (datos) => {
    try {
      const { data, status, statusText } = await axios.post(
        URL + "/autenticar",
        datos
      );
      setInfo(data);
      console.log(data);
    } catch (error) {
      //console.log(error);
      setError(error);
    }
  };
  return { info, error, execute: callApi };
};

export const IngresarSistema = async (data) => {
  return await axios.post(URL + "/autenticar", data).then((response) => {
    //retonrna solo el axios
    console.log(response);
    if (response.data && response.data.token) {
      // TODO   ---- cookies -- varibles de sesion - sesion storage
      //local storage
      const session = Sesion(response.data.token);
      console.log("SESSION INGRESAR SISTEMA", session);
    }
    return response.data; //retonrna lo q se quiere reciibr
  });
};

export const RegistrarEquipo = async (data) => {
  console.log('EquipoGuardar ** '+ObtenerSesion());
  console.log("DATA EquipoGuardar", data);
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  return await axios.post(URL + '/equipos/guardar', data, configuracion).then((response) => {
      console.log('EquipoGuardar -------');
      console.log(response);
      return response.data;
  });
}

export const RegistrarOrden = async (data) => {
  console.log('RegistrarOrden ** '+ObtenerSesion());
  console.log("DATA EquipoGuardar", data);
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  return await axios.post(URL + '/orden/guardar', data, configuracion).then((response) => {
      console.log('RegistrarOrden -------');
      console.log(response);
      return response.data;
  });
}

export const ObtenerPersona = async (external) => {
  console.log('Personas');
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  return await axios.get(URL + '/personas/obtener/' + external, configuracion).then((response) => {
    console.log("response Persona", response);
    return response.data;
  });
}

export const Personas = async () => {
  console.log('Personas');
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  console.log('Personas'+' '+getSession());
  return await axios.get(URL + '/personas', configuracion).then((response) => {
      console.log(response);
      return response.data;
  });
}

export const Equipos = async () => {
  console.log('Equipos');
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  console.log('Equipos'+' '+getSession());
  return await axios.get(URL + '/equipos', configuracion).then((response) => {
      console.log(response);
      return response.data;
  });
}

export const Marcas = async () => {
  console.log('Marcas');
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  console.log('Marcas'+' '+getSession());
  return await axios.get(URL + '/marcas', configuracion).then((response) => {
      console.log(response);
      return response.data;
  });
}

export const Clientes = async () => {
  console.log('Clientes');
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  console.log('Clientes'+' '+getSession());
  return await axios.get(URL + '/clientes', configuracion).then((response) => {
      console.log(response);
      return response.data;
  });
}

export const Identificacion = async () => {
  console.log('Identificacion');
  const configuracion = {headers: {
      'Authorization': getSession()
  }};
  console.log('Identificacion'+' '+getSession());
  return await axios.get(URL + '/identificacion', configuracion).then((response) => {
      console.log(response);
      return response.data;
  });
}

export const RegistrarPersona = async (data,external) => {
  console.log('PersonaGuardar ** '+ObtenerSesion());
  console.log(data);
  const configuracion = {headers: {
      'Authorization': ObtenerSesion()
  }};
  return await axios.post(URL + '/personas/guardar', data, configuracion,external).then((response) => {
      console.log('PersonaGuardar -------');
      console.log(response);
      return response.data;
  });
}

export const RegistrarCliente = async (data) => {
    const configuracion = {headers: {
      'Authorization': getSession()
  }};
  return await axios.post(URL + "/personas/guardar/cliente", data,configuracion).then((response) => {
    console.log("resp RegistrarCliente", response);
    return response.data;
  })
}


export const CambiarLibro = async (external) => {
  const config = {
    headers: {
      "access-token": ObtenerSesion(),
    }
  }
  return await axios.get(URL + "/libro/cambio/"+external, config).then((response) => {
    console.log("resp cambiarLibro", response);
    return response.data;
  })
}

export const ModificarLibro = async (data) => {
  const token = {
    headers: {
      "access-token": ObtenerSesion(),
    }
  }
  console.log('data del modificar', data);
  return await axios.post(URL + "/libro/editar",data,token).then((response) => {
    console.log("resp modificar Libro", response.data);
    return response.data;
  })
}



export const InicioSesion = (data, accion = true) => {
    const [info, setInfo] = useState(null);
    const[error, setError] = useState(null);
    useEffect(() => {
        if (accion) callApi(data);
    },[]);
    const callApi = async (datos) => {
        try {
            const {data, status, statusText} = 
            await axios.post(URL+'/inicio_sesion', datos);
            setInfo(data);
            console.log(data);
        } catch (error) {
            //console.log(error);
            setError(error);
        }
    }
    return {info, error, execute: callApi};
};


export const CerrarSistema = async (data) => {
   /* return await axios.post(URL+'/autenticar', data).then((response)=>{
        //aqui guardamos la variable de sesion
        console.log(response);
        if(response.data && response.data.token){
            //el responde.data es de axios 
            //aqui uso cualquier varible de sesion, cookies, sesion storage, local storage, variables de sesion
            //guardo el token en la variable session
            const session = Session(response.data.token);
        }
        return response.data;
    });*/
    await CerrarSession();
    return true;
}

export const ObtenerOrdenEstado = async (fecha, estado) => {
    const config = {headers: {
        'access-token': ObtenerSession()
    }}; 
    return await axios.get(URL+'/ordenes/buscar/'+fecha+'/'+estado, config).then((response)=>{
        console.log(response);
        return response.data;
    });
}
export const ObtenerOrdenIdentificacion = async (identificacion) => {
    const config = {headers: {
        'access-token': ObtenerSession()
    }}; 
    return await axios.get(URL+'/ordenes/buscarIdentificacion/'+identificacion, config).then((response)=>{
        console.log(response);
        return response.data;
    });
}
export const ObtenerOrdenNombre = async (nombre) => {
    const config = {headers: {
        'access-token': ObtenerSession()
    }}; 
    return await axios.get(URL+'/ordenes/buscarNombre/'+nombre, config).then((response)=>{
        console.log(response);
        return response.data;
    });
}
export const ObtenerOrdenFecha = async (fechaInicio, fechaFin) => {
    const config = {headers: {
        'access-token': ObtenerSession()
    }};
    return await axios.get(URL+'/ordenes/buscarF/'+fechaInicio+'/'+fechaFin,config).then((response)=>{
        console.log(response.data);
        return response.data;
    });
}

export const ListarFacturas = async () => {
    const config = {headers: {
        'access-token': ObtenerSession()
    }};
    return await axios.get(URL+'/ordenes/listar',config).then((response)=>{
        console.log(response.data);
        return response.data;
    })
}

export const ObtenerOrdenExternal = async (external_id) => {
    const config = {headers: {
        'access-token': ObtenerSession()
    }};
    return await axios.get(URL+'/ordenes/'+external_id,config).then((response)=>{
        console.log(response.data);
        return response.data;
    })
}
