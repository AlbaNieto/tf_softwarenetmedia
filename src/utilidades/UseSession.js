import React from "react";

export const Sesion = (token) => {
  localStorage.setItem("token", token); //funciona en la misma página
};
export const ObtenerSesion = (token) => {
  return localStorage.getItem("token"); //funciona en la misma página
};

export const sessionToken = (token) => {
  localStorage.setItem('token', token);
}
export const getSession = () => {
  return localStorage.getItem('token');
}

export const borrarSessionToken = () => {
  console.log("borrarSessionToken");
  localStorage.clear();
  
}

export const EstaSession = () => {
  const token = localStorage.getItem('token');
  
  if (token && token != 'undefined') return true;
  else return false;
}


//export const EstaSession = () => {
  //const token = localStorage.getItem("token"); //si existe
  //if (token) {
 //   return true;
 // } else {
 //   return false;
 // }
//};

export const CerrarSession = () => {
  localStorage.clear();
  localStorage.removeItem("token");
  //todo
  //llaado al servicio web para borrar token
};
