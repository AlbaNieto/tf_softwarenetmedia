import React from 'react';
import '../css/Bootstrap.css';
import '../css/principal.css';
import { useState } from "react";
import swal from "sweetalert";
import { InicioSesion } from "../hooks/ConexionSw";
import { useForm } from "react-hook-form";
import Modal from 'react-modal';
import { sessionToken } from "../utilidades/UseSession";
import { useNavigate, Link } from 'react-router-dom';
import { CerrarSession } from "../utilidades/UseSession";

const mensajeError = (texto) => swal({
    title: "Error",
    text: texto,
    icon: "error",
    button: "Aceptar",
    timer: 10000
});
const MensajeOk = (texto) => swal({
    title: "Ok",
    text: texto,
    icon: "success",
    button: "Aceptar",
    timer: 10000
});

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: '40%',
        bottom: 'auto',
        marginRight: '-15%',
        transform: 'translate(-50%, -50%)',
    },
};

const estiloPagina = {
    content: {
        top: '50%',
        left: '50%',
        right: '40%',
        bottom: 'auto',
        marginRight: '-100%',
        transform: 'translate(-50%, -50%)',
    },
};

const Principal = () => {
    console.log('data session');
    const { register, handleSubmit, formState: { errors } } = useForm();
    const navegacion = useNavigate();
    const [info, setInfo] = useState(undefined);
    const [llamada, setLlamada] = useState(false);
    const [modalIsOpen, setIsOpen] = React.useState(false);

    CerrarSession();

    const onSubmit = (data) => {
        var datos = {
            'correo': data.correo,
            'clave': data.clave,
        };
        InicioSesion(datos).then((info) => {
            if (info && info.data.token) {
                console.log("ROL info", info.data.rol);
                if (info.data.rol == "gerente") {
                    sessionToken(info.data.token);
                    navegacion('/Registro');
                }else{
                    mensajeError('Solo el gerente puede agregar personas');
                }
            } else {
                if (info && info.msg) {//cuado sale un error de trasaccion
                    mensajeError(info.msg);
                } else {
                    //cuando sale erro d evalidacion en backend
                    mensajeError('Error de validacion');
                }

            }
        }, (error) => {
            mensajeError(error.message);
            console.log(error);

        });
    }

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
        window.location.href = window.location.href;
    }

    return (
        <div className="container h-100" style={{estiloPagina}}>
            <div className="d-flex justify-content-center mt-3">
                <div className="card mt-10 col-md-6 animated bounceInDown myForm">
                    <div className="card-header">
                        <h4 className='Title-center'>Bienvenidos a NETMEDIA</h4>
                    </div>
                    <div className="card-footer">

                        <button onClick={openModal} className="btn btn-success btn-sm float-length submit_btn "><i class="fas fa-arrow-alt-circle-length"></i><Link className='nav-link' style={{ color: 'cyan' }}>Registro</Link></button>

                        <button className="btn btn-success btn-sm float-right submit_btn"><i class="fas fa-arrow-alt-circle-right"></i><Link to="/Iniciar_Sesion" className='nav-link' style={{ color: 'cyan' }}>Login</Link></button>

                    </div>
                </div>
            </div>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
                data={{ backgroud: "green" }}
                ariaHideApp={false}
            >
                <div style={{ float: "right", margin: "-10px" }}>
                    <svg onClick={closeModal} xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                    </svg>
                </div>
                <h3 style={{ textAlign: "center", fontWeight: "bold" }}>Login Para Registrar</h3>
                <div>
                    <div className="card-body">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="input-group form-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-user"></i></span>
                                </div>
                                <input {...register('correo', { required: true, pattern: /^\S+@\S+$/i })} type="text" className="form-control" placeholder="Usuario" />
                                {errors.correo && errors.correo.type === 'required' && <div className="alert alert-danger">Ingrese el correo</div>}
                                {errors.correo && errors.correo.type === 'pattern' && <div className="alert alert-danger">Ingrese un correo valido</div>}
                            </div>
                            <div className="input-group form-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-key"></i></span>
                                </div>
                                <input {...register('clave', { required: true })} type="password" className="form-control" placeholder="Clave" />
                                {errors.clave && errors.clave.type === 'required' && <div className="alert alert-danger">Ingrese el dato</div>}
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn center login_btn">Registrar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </Modal>
        </div>




    );
};

export default Principal;