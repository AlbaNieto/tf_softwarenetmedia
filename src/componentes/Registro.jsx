import '../css/Bootstrap.css';

import React from 'react'
import { useForm } from 'react-hook-form';
import swal from 'sweetalert';
import { Identificacion, RegistrarPersona } from '../hooks/ConexionSw';
import { useNavigate, Link } from "react-router-dom";
import { validarCedula, validarPasaporte, validarRUC } from '../utilidades/Validaciones';
import { useState } from "react";

const mensajeError = (texto) => swal({
    title: "Error",
    text: texto,
    icon: "error",
    button: "Aceptar",
    timer: 10000
});
const MensajeOk = (texto) => swal({
    title: "Ok",
    text: texto,
    icon: "success",
    button: "Aceptar",
    timer: 10000
});

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: '40%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

const Registro = () => {
    const navegacion = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [info, setInfo] = useState(undefined);
    const [llamada, setLlamada] = useState(false);

    if (!llamada) {
        Identificacion().then((data) => {
            setLlamada(true);
            console.log(data);
            setInfo(data);
            console.log("=========================");
            const identificaciones = data.data.map((element) => element.identificacion);
            console.log(identificaciones);
        },
            (error) => {
                console.log(error);
                mensajeError(error.message);
            }
        );
    }

    const onSubmit = (data) => {
        var datos = {
            'nombres': data.nombres,
            'apellidos': data.apellidos,
            'direccion': data.direccion,
            'telefono': data.telefono,
            'tipo': data.tipo,
            'identificacion': data.identificacion,
            'tipo_persona': data.tipo_persona,
            'cuenta': {
                'correo': data.correo,
                'clave': data.clave
            }
        }
        console.log("INFOOOOOOOOO BAJO SUBMIT", info.data);

        if (info && info.data && info.data.some(element => element.identificacion === data.identificacion)) {
            mensajeError("La identificación ingresada se encuentra registrada en la base de datos");
        } else if (info && info.data && info.data.some(element => element.correo === data.correo)) {
            mensajeError("La dirección de correo ingresada ya está registrada en la base de datos");
        } else {
            if (data.tipo == 'CEDULA', "RUC", "PASAPORTE") {
                const registros = RegistrarPersona(datos).then((info) => {
                    if (validarCedula(data.identificacion) || validarRUC(data.identificacion) || validarPasaporte(data.identificacion)) {
                        console.log("info REGISTRO", info);
                        if (info) {
                            MensajeOk("msg: " + info.msg + "\n" + "\n" + "code: " + info.code);
                            window.location.reload();
                        }
                    } else {
                        mensajeError("Identificación no valida");
                    }
                }, (error) => {
                    mensajeError(error.message);
                });
            }
        }


        /**if (data.tipo == 'CEDULA', "RUC", "PASAPORTE") {
            const registros = RegistrarPersona(datos).then((info) => {
                if (validarCedula(data.identificacion) || validarRUC(data.identificacion) || validarPasaporte(data.identificacion)) {
                    console.log("info REGISTRO", info);
                    if (info) {
                        console.log("session token", info.token);
    
                        MensajeOk("msg: " + info.msg + "\n" + "\n" + "code: " + info.code);
                    }
                } else {
                    mensajeError("Identificación no valida");
                }
            }, (error) => {
                mensajeError(error.message);
            });
        }*/

    }

    var cantidadDeClaves = Object.keys(errors).length;
    return (
        <div style={{ margin: "30px" }}>
            <h2 style={{ fontWeight: "bold", fontFamily: "monospace", fontStyle: "-moz-initial" }} className="card-title text-center mb-4 pb-2 border-bottom">REGISTRAR PERSONA</h2>
            <div className="container">
                {errors && cantidadDeClaves > 0 && (
                    <p className="alert alert-danger">Debe llenar todos los datos de la persona</p>
                )}
                <form onSubmit={handleSubmit(onSubmit)} className="row g-3 mt-4">
                    <div className="col-md-6">
                        <label htmlFor="nombres" className="form-label" style={{ fontWeight: "bold" }}>Nombres</label  >
                        <input id="nombres" {...register('nombres', { required: true })} type="text" className="form-control" />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="apellidos" className="form-label" style={{ fontWeight: "bold" }}>Apellidos</label>
                        <input id="apellidos" {...register('apellidos', { required: true })} type="text" className="form-control" />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="direccion" className="form-label" style={{ fontWeight: "bold" }}>Direccion</label>
                        <input id="direccion" {...register('direccion', { required: true })} type="text" className="form-control" />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="telefono" className="form-label" style={{ fontWeight: "bold" }}>Telefono</label>
                        <input id="telefono" {...register('telefono', { required: true })} type="text" className="form-control" />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="tipo" className="form-label" style={{ fontWeight: "bold" }}>Tipo de identificacion</label>
                        <div>
                            <select className="form-select" {...register("tipo", { required: true })}>
                                <option value="" disabled>Select Option</option>
                                <option value="CEDULA">CEDULA</option>
                                <option value="RUC">RUC</option>
                                <option value="PASAPORTE">PASAPORTE</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="identificacion" className="form-label" style={{ fontWeight: "bold" }}>Identificacion</label>
                        <input id="identificacion" {...register('identificacion', { required: true })} type="text" className="form-control" />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="tipo_persona" className="form-label" style={{ fontWeight: "bold" }}>Rol</label>
                        <div>
                            <select className="form-select" {...register("tipo_persona", { required: true })}>
                                <option value="" disabled>Select Option</option>
                                <option value="admin">Administrador</option>
                                <option value="gerente">Gerente</option>
                                <option value="usuario">Empleado de la empresa</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="correo" className="form-label" style={{ fontWeight: "bold" }}>Correo</label>
                        <input id="correo" {...register('correo', { required: true })} type="email" className="form-control" />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="clave" className="form-label" style={{ fontWeight: "bold" }}>Clave</label>
                        <input id="clave" {...register('clave', { required: true })} type="password" className="form-control" />
                    </div>
                    <div className="col-12">
                        <div className="btn-group" role="group">
                            <button style={{ margin: "10px" }} type='submit' class='btn btn-primary btn-guardar'>Guardar{""}</button><div>{""}</div>
                            <Link style={{ margin: "10px" }} to={'/'} id='buttonRegresar' type='submit' class='btn btn-primary btn-regresar'>Regresar</Link>
                            <Link style={{ margin: "10px" }} to={'/InicioG'} id='buttonRegresar' type='submit' class='btn btn-primary btn-ingresar'>IngresarSistema</Link>
                        </div>
                    </div>
                </form>
            </div >
        </div>
    )
}

export default Registro;
