import React from 'react';
import DatePicker from 'react-datepicker';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import '../css/Bootstrap.css'
import "react-datepicker/dist/react-datepicker.css";
import NavBarGerente from './NavBarGerente';
import { ObtenerOrdenEstado, ObtenerOrdenIdentificacion, ObtenerOrdenNombre, ObtenerOrdenFecha } from '../hooks/ConexionSw';

const ReparacionesView = () => {
    const { handleSubmit } = useForm();
    const [fechaInicio, setStartDate] = useState(new Date());
    const [fechaFinal, setEndDate] = useState(new Date());
    const [llamada, setLlamada] = useState(false);
    const [info, setInfo] = useState(undefined);
    
    
    const onSubmit = () => {
        if (!llamada) {
            if (document.getElementById('cbox1').checked) {
                var nombre = document.getElementById("inputBusqueda").value
                
                const datos = ObtenerOrdenNombre(nombre).then((data) => {
                    setLlamada(true);
                    setInfo(data);
                    
                }, (error) => {
                    console.log(error);
                    //mensaje(error.message);
                });
            }
            if (document.getElementById('cbox2').checked) {
                var identificacion = document.getElementById("inputBusqueda").value
                
                const datos = ObtenerOrdenIdentificacion(identificacion).then((data) => {
                    setLlamada(true);
                    setInfo(data);
                    
                }, (error) => {
                    console.log(error);
                    //mensaje(error.message);
                });
            }
            if (document.getElementById('cbox3').checked) {
                var estado = document.getElementById("inputBusqueda").value
                
                const datos = ObtenerOrdenEstado(fechaInicio, estado).then((data) => {
                    setLlamada(true);
                    setInfo(data);
                    
                }, (error) => {
                    console.log(error);
                    //mensaje(error.message);
                });
            }
            if (document.getElementById('cbox4').checked) {
                var fechaI = document.getElementById("fechaInicio").value;
                var fechaF = document.getElementById("fechaFin").value;
                const datos = ObtenerOrdenFecha(fechaI,fechaF).then((data) => {
                    //setLlamada(true);
                    setInfo(data);                    
                }, (error) => {
                    console.log(error);
                    //mensaje(error.message);
                });
            }
        }
    };
    return (
        <div onSubmit={handleSubmit(onSubmit)}>
            <NavBarGerente />
            <div className='container'>
                <h1 className='text-center' style={{ color: "#018f8f", fontWeight: 'bold' }}>BUSQUEDA DE REPARACIONES </h1>
            </div>
            <label></label>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="container" style={{ display: "flex" }}>
                    <label style={{ whiteSpace: 'nowrap' }}>Fecha de inicio:</label>
                    <DatePicker id='fechaInicio' selected={fechaInicio} onChange={(date) => setStartDate(date)} dateFormat='dd-MM-yyyy'/>
                    <label style={{ whiteSpace: 'nowrap' }}>Fecha de final:</label>
                    <DatePicker id='fechaFin' selected={fechaFinal} onChange={(date) => setEndDate(date)} dateFormat='dd-MM-yyyy'/>
                    <button className="btn btn-success" type="submit" style={{ backgroundColor: "#018f8f" }}>Buscar</button>
                </div>
                <div className="container">
                    <input type="BUSCAR ORDENES" className="form-control" id="inputBusqueda" />
                    <label><input type="checkbox" id="cbox1" value="nombre_checkbox" /> Nombre</label>
                    <label><input type="checkbox" id="cbox2" value="nombre_checkbox" /> Identificacion</label>
                    <label><input type="checkbox" id="cbox3" value="nombre_checkbox" /> Estado</label>
                    <label><input type="checkbox" id="cbox4" value="nombre_checkbox" /> Fecha</label>
                </div>
            </form>
            <label></label>
            <div className='container'>
                <table className='table table-hover'>
                    <thead style={{ color: "#018f8f" }}>
                        <tr>
                            <th>Cliente</th>
                            <th>Nro Factura</th>
                            <th>Estado</th>
                            <th>Sub Total Iva</th>
                            <th>Total</th>
                            <th>Equipo Electronico</th>
                        </tr>
                    </thead>
                    <tbody id='contenidoTabla' style={{ color: "#ffffff"}}>
                    {info && info.data && info.data.map((element, key) => {
                            return <tr key={key}>
                                <td>{element.cliente}</td>
                                <td>{element.nroFactura} </td>
                                <td>{element.estado}</td>
                                <td>{element.subTotalIva}</td>
                                <td>{element.total}</td>
                                <td>{element.equipoElectronico}</td> 
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default ReparacionesView;