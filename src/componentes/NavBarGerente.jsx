import React from 'react';
import { Link } from 'react-router-dom';
const NavBarGerente = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-lg bg-dark navbar-dark">
                <div className="container">
                    <a className="navbar-brand font-weight-bold font-italic" style={{color: 'cyan'}}>
                    <Link to="/InicioG" className='nav-link' style={{color: 'cyan'}} >NETMEDIA</Link>
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                            <a className="nav-link " href="">
                            <Link to="/Agendar_Reparacion" className='nav-link'>Agendar Reparación</Link>
                            </a>
                            <a className="nav-link" href="">
                            <Link to="/Gestion_Fechas" className='nav-link'>Gestionar Fechas</Link>
                            </a>
                            <a className="nav-link" href="">
                            <Link to="/Buscar" className='nav-link'>Buscar Reparaciones</Link>
                            </a>
                            <a className="nav-link" href="">
                            <Link to="/Facturacion" className='nav-link'>Facturar Reparaciones</Link>
                            </a>
                            
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    );
};

export default NavBarGerente;