export const validarCedula = (cedula) => {
  // Comprobar que la cédula tenga 10 dígitos
  if (cedula.length !== 10) {
    return false;
  }

  // Calcular el dígito verificador
  const digitoVerificador = parseInt(cedula.charAt(9));
  const coeficientes = [2, 1, 2, 1, 2, 1, 2, 1, 2];
  let suma = 0;
  for (let i = 0; i < 9; i++) {
    const valor = parseInt(cedula.charAt(i)) * coeficientes[i];
    suma += valor > 9 ? valor - 9 : valor;
  }
  const resultado = (10 - (suma % 10)) % 10;

  // Comprobar que el dígito verificador sea correcto
  return digitoVerificador === resultado;
}

export const validarRUC = (ruc) => {
    // Verificar que el RUC tenga 13 dígitos
    if (ruc.length !== 13) {
      return false;
    }
  
    // Verificar que los primeros 10 dígitos sean un número de cédula válido
    const cedula = ruc.slice(0, 10);
    if (!validarCedula(cedula)) {
      return false;
    }
  
    // Verificar que los últimos 3 dígitos sean un número de secuencia válido
    const secuencia = parseInt(ruc.slice(10, 13));
    if (isNaN(secuencia) || secuencia < 1 || secuencia > 999) {
      return false;
    }
  
    // Si todos los criterios de validación son verdaderos, el RUC es válido
    return true;
  }

  export const validarPasaporte = (pasaporte) => {
    const regex = /^[a-zA-Z]\d{2}[a-zA-Z0-9]{6}$/;
    return regex.test(pasaporte);
  }
