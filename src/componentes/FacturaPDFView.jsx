import { PDFViewer, Text, View, Document, Page } from '@react-pdf/renderer';
import React, { Fragment, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';
import { ObtenerOrdenExternal } from '../hooks/ConexionSw';

const FacturaPDFView = () => {
    const [llamada, setLlamada] = useState(false);
    const { external } = useParams();
    const [info, setInfo] = useState(undefined);
    const [numFilasTabla, setNumFilasTabla] = useState(17);

    if (!llamada) {
        const datos = ObtenerOrdenExternal(external).then((data) => {
            setValue("nroFactura", data.data.nroFactura);
            setInfo(data);
            setLlamada(true);
        }, (error) => {
            console.log(error);
            //mensaje(error.message);
        });
    }
    const filasVacias = Array(numFilasTabla).fill(0)
    return (
        <div>
            <PDFViewer style={{ width: window.innerWidth, height: window.innerHeight }}>
                <Document>
                    <Page size="A4" style={{
                        fontFamily: 'Helvetica-Oblique', fontSize: 11, lineHeight: 1.5,
                        flexDirection: 'column'
                    }}>
                        <View style={{ margin: 20, padding: 20, marginTop: 36, flexDirection: 'row' }}>
                            {info && info.data && <Text>NRO. Factura {info.data.nroFactura}</Text>}
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ marginLeft: '75%' }}>NETMEDIA</Text>
                                <Text style={{ marginLeft: '70%' }}>Servicios Informáticos</Text>
                            </View>
                        </View>
                        <View style={{ margin: 20, padding: 20 }}>
                            <View>
                                {info && info.data && <Text>Cédula/Ruc/Pasaporte: {info.data.identificacion}</Text>}
                                {info && info.data && <Text>Nombres: {info.data.nombres}</Text>}
                                {info && info.data && <Text>Apellidos: {info.data.apellidos}</Text>}
                                {info && info.data && <Text>Dirección: {info.data.direccion}</Text>}
                                {info && info.data && <Text>Teléfono: {info.data.telefono}</Text>}
                                {info && info.data && <Text>Dispositivo: {info.data.dispositivo}</Text>}
                            </View>
                            <View id='CabeceraTabla' style={{
                                flexDirection: 'row', borderBottomColor: '#000000', backgroundColor: '#000000',
                                borderBottomWidth: 1, height: 24, alignItems: 'center', flexGrow: 1, marginTop: "2%", color: 'white',
                            }}>
                                <Text style={{ width: '10%', borderRightColor: '#000000', borderRightWidth: 1 }}>Cantidad</Text>
                                <Text style={{ width: '60%', borderRightColor: '#000000', borderRightWidth: 1 }}>Descripción</Text>
                                <Text style={{ width: '15%', borderRightColor: '#000000', borderRightWidth: 1 }}>V. Unitario</Text>
                                <Text style={{ width: '15%', borderRightColor: '#000000', borderRightWidth: 1 }}>V. Total</Text>
                            </View>
                            <Fragment>
                            {info && info.data && info.data.detalles.map((element, key) => {
                                return <View key={key} id='CuerpoTabla' style={{
                                        flexDirection: 'row', borderBottomColor: '#000000',
                                        borderBottomWidth: 1, height: 24, alignItems: 'center', color: 'black'
                                    }}>
                                        <Text style={{ width: '10%', borderRightColor: '#000000', borderRightWidth: 1, paddingLeft: 8 }}>{element.cantidad}</Text>
                                        <Text style={{ width: '60%', borderRightColor: '#000000', borderRightWidth: 1, paddingLeft: 8 }}>{element.descripcion}</Text>
                                        <Text style={{ width: '15%', borderRightColor: '#000000', borderRightWidth: 1, paddingLeft: 8 }}>{element.pu}</Text>
                                        <Text style={{ width: '15%', borderRightColor: '#000000', borderRightWidth: 1, paddingLeft: 8 }}>{element.pt}</Text>
                                    </View>
                            })}
                            </Fragment>
                            <View id='footer'>
                                <View style={{
                                    flexDirection: 'row', borderBottomColor: '#000000',
                                    borderBottomWidth: 1, height: 24, alignItems: 'center'
                                }}>
                                    <Text style={{ width: '85%', textAlign: 'right', borderRightColor: '#000000', borderRightWidth: 1, paddingRight: 8 }}>SUB-Total</Text>
                                    {info && info.data && <Text style={{ paddingLeft: 8 }}>{info.data.subtotal}</Text>}
                                </View>
                                <View style={{
                                    flexDirection: 'row', borderBottomColor: '#000000',
                                    borderBottomWidth: 1, height: 24, alignItems: 'center'
                                }}>
                                    <Text style={{ width: '85%', textAlign: 'right', borderRightColor: '#000000', borderRightWidth: 1, paddingRight: 8 }}>TOTAL</Text>
                                    {info && info.data && <Text style={{ paddingLeft: 8 }}>{info.data.total}</Text>}
                                </View>
                            </View>
                        </View>
                    </Page>
                </Document>
            </PDFViewer>
        </div>
    );
};

export default FacturaPDFView;