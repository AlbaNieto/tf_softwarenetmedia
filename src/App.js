import './App.css';
import InicioEmpleado from './componentes/InicioEmpleado';
import InicioGerente from './componentes/InicioGerente';
import AgendarReparacion from './componentes/AgendarReparacion';
import {Login} from './componentes/Login';
import FacturacionView from './componentes/FacturacionView';
import ListarAgenda from './componentes/ListarAgenda';
import Registro from './componentes/Registro';
import ReparacionesView from './componentes/ReparacionesView';
import RevisarRegistros from './componentes/RevisarRegistros';
import Principal from './componentes/Principal';
import AgregarEquipo from './componentes/AgregarEquipo';
import FacturaPDFView from './componentes/FacturaPDFView';
import { BrowserRouter as Router, Link, Route, Routes } from 'react-router-dom';
import AgregarOrden from './componentes/AgregarOrden';
function App() {
  return (
      <Routes>
        <Route path="/" element={ <Principal />} exact />
        <Route path="/Iniciar_Sesion" element={ <Login />}></Route>
        <Route path='/Agendar_Reparacion' element={<AgendarReparacion />}></Route>
        <Route path="/Registrar/:external" element={<AgregarEquipo />}></Route>
        <Route path='/Facturacion' element={<FacturacionView />}></Route>
        <Route path='/Factura/:external' element={<FacturaPDFView />}></Route>
        <Route path='/InicioE' element={<InicioEmpleado />}></Route>
        <Route path='/InicioG' element={<InicioGerente />}></Route>
        <Route path='/Gestion_Fechas' element={<AgregarOrden />}></Route>
        <Route path='/Registro' element={<Registro />}></Route>
        <Route path='/Buscar' element={<ReparacionesView />}></Route>
        
      </Routes>
  );
}

export default App;
