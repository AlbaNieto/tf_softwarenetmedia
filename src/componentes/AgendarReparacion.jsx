import React from 'react';
import swal from "sweetalert";
import { useState } from "react";
import Modal from 'react-modal';
import { RegistrarPersona, Clientes } from '../hooks/ConexionSw';
import { useNavigate, Link } from "react-router-dom";
import { useForm } from 'react-hook-form';
import NavBarGerente from './NavBarGerente';

const mensajeError = (texto) => swal({
    title: "Error",
    text: texto,
    icon: "error",
    button: "Aceptar",
    timer: 10000
});
const MensajeOk = (texto) => swal({
    title: "Ok",
    text: texto,
    icon: "success",
    button: "Aceptar",
    timer: 10000
});

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: '40%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};


const AgendarReparacion = () => {


    const [info, setInfo] = useState(undefined);
    const [llamada, setLlamada] = useState(false);
    const [search, setSearch] = useState("");

    if (!llamada) {
        const datos = Clientes().then((data) => {
            setLlamada(true);
            setInfo(data);
            console.log("INFO", info);
            console.log("ROL-----", data.rol)
        },
            (error) => {
                console.log(error);
                mensajeError(error.message);
            }
        );
    }

    const [modalIsOpen, setIsOpen] = React.useState(false);

    function openModal() {
        setIsOpen(true);
    }


    function closeModal() {
        setIsOpen(false);
        window.location.href = window.location.href;
    }
    const navegacion = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = (data) => {
        console.log("ROL----->", data.rol);
        var datos = {
            'nombres': data.nombres,
            'apellidos': data.apellidos,
            'direccion': data.direccion,
            'telefono': data.telefono,
            'tipo': data.tipo,
            'identificacion': data.identificacion,
            'rol': data.rol
        }

        const registros = RegistrarPersona(datos).then((info) => {
            console.log("info REGISTRO", info);
            if (info) {
                console.log("session token", info.token);

                MensajeOk("msg: " + info.msg + "\n" + "\n" + "code: " + info.code);
            }
        }, (error) => {
            mensajeError(error.message);
        });

    }

    // Función para filtrar la información según el valor de búsqueda
    const filtrarInfo = () => {
        if (!info) {
            return [];
        }
        if (!search) {
            return info.data;
        }
        return info.data.filter((element) =>
            element.identificacion.toLowerCase().includes(search.toLowerCase())
        );
    };

    // Función para manejar cambios en el campo de entrada de texto de búsqueda
    const handleSearch = (event) => {
        setSearch(event.target.value);
    };


    var cantidadDeClaves = Object.keys(errors).length;

    return (
        <div className="">
            <NavBarGerente />
            <div className=""></div>
            <div className="">
                <div className="card text-center">
                    <div className="card-body">
                        <h5 className="card-title alert alert-info">AGENDAR REPARACION</h5>
                        <div className='card'>
                            <div className='card-body'>
                                <button onClick={openModal} type="submit" className='btn btn-success'>Nuevo</button>
                            </div>
                        </div>
                        <div>
                            <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '10px' }}>
                                <div style={{ width: '50%' }}>
                                    <input
                                        type="text"
                                        placeholder="Buscar por Identificación"
                                        value={search}
                                        onChange={handleSearch}
                                        style={{
                                            width: '100%',
                                            height: '40px',
                                            borderRadius: '5px',
                                            padding: '5px 10px',
                                            fontSize: '16px',
                                            border: '1px solid #ccc',
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div style={{ width: "55% ", margin: "30px auto", border: "1px solid black", borderRadius: "5px", height: "600px", overflow: "auto" }}>
                            <table className="table table-striped table-bordered" style={{ width: "100%", margin: "0px auto", border: "1px solid black", borderRadius: "5px" }}>
                                <thead className="thead-dark">
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Direccion</th>
                                        <th>Telefono</th>
                                        <th>Identificacion</th>
                                        <th>Rol Persona</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {filtrarInfo().map((element, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{key + 1}</td>
                                                <td>{element.nombres}</td>
                                                <td>{element.apellidos}</td>
                                                <td>{element.direccion}</td>
                                                <td>{element.telefono}</td>
                                                <td>{element.identificacion}</td>
                                                <td>{element.rol}</td>
                                                <td>
                                                    <Link
                                                        to={"/Registrar/" + element.external} className="btn btn-warning">Registrar
                                                    </Link>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    style={{
                        content: {
                            backgroundColor: '#ffffff',
                            borderRadius: '5px',
                            boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.1)',
                            margin: 'auto',
                            maxWidth: '600px',
                            padding: '30px',
                        },
                        overlay: {
                            backgroundColor: 'rgba(0, 0, 0, 0.3)',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        },
                    }}
                >
                    <div style={{ float: "right", margin: "-10px" }}>
                        <svg onClick={closeModal} xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </div>
                    <h2 style={{ textAlign: "center", fontWeight: "bold" }}>Agregar Clientes</h2>
                    <div>
                        {errors && cantidadDeClaves > 0 && <p className="alert alert-danger">Faltan datos por llenar</p>}
                        <div className='container'>
                            <form onSubmit={handleSubmit(onSubmit)} className='container' style={{ margin: "20px" }}>
                                <div className="form-group">
                                    <label className='form-label'>Nombres</label>
                                    <input id="nombres" {...register('nombres', { required: true })} type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label className='form-label'>Apellidos</label>
                                    <input id="apellidos" {...register('apellidos', { required: true })} type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label className='form-label'>Direccion</label>
                                    <input id="direccion" {...register('direccion', { required: true })} type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label className='form-label'>Telefono</label>
                                    <input id="telefono" {...register('telefono', { required: true })} type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label className='form-label'>Tipo Identificacion</label>
                                    <input id="tipo" {...register('tipo', { required: true })} type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label className='form-label'>Identificacion</label>
                                    <input id="identificacion" {...register('identificacion', { required: true })} type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label className='form-label'>Tipo Persona</label>
                                    <input id="tipo_persona" {...register('tipo_persona', { required: true })} type="text" className="form-control" />
                                </div>
                                <div>
                                    <button type='submit' className='btn btn-primary'>Guardar</button>
                                    <Link to={'/Agendar_Reparacion'} id='buttonRegresar' type='submit' style={{ float: "right" }} className='btn btn-primary'>Regresar</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </Modal>
            </div>
        </div>
    );
};

export default AgendarReparacion;
