import swal from "sweetalert";
import Modal from 'react-modal';
import { RegistrarOrden, Equipos } from '../hooks/ConexionSw';
import { useNavigate, Link } from "react-router-dom";
import { useForm } from 'react-hook-form';
import NavBarGerente from './NavBarGerente';
import React, { useState, useEffect } from 'react';

const mensajeError = (texto) => swal({
    title: "Error",
    text: texto,
    icon: "error",
    button: "Aceptar",
    timer: 10000
});
const MensajeOk = (texto) => swal({
    title: "Ok",
    text: texto,
    icon: "success",
    button: "Aceptar",
    timer: 10000
});

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: '40%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};


const AgregarOrden = () => {


    const [info, setInfo] = useState(undefined);
    const [llamada, setLlamada] = useState(false);
    const [search, setSearch,setFiltrarInfo] = useState("");

    if (!llamada) {
        const datos = Equipos().then((data) => {
            setLlamada(true);
            setInfo(data);
            console.log("INFO", info);
            console.log("ROL-----", data.rol)
        },
            (error) => {
                console.log(error);
                mensajeError(error.message);
            }
        );
    }
    const [equipos, setEquipos, setOrden] = useState([]);

    useEffect(() => {
        Equipos().then((data) => {
            if (data) {
                setEquipos(data.data);
                console.log("EQUIPOS DATA", data.data);
            }
        },
            (error) => {
                mensajeError(error.message);
            }
        );
    }, []);

    const [modalIsOpen, setIsOpen] = React.useState(false);

    function openModal() {
        setIsOpen(true);
    }


    function closeModal() {
        setIsOpen(false);
        window.location.href = window.location.href;
    }
    const navegacion = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = (data) => {
        console.log("ROL----->", data.external_id);
        var datos = {
            'external_equipo': data.Equipo_external,
            external_equipo: data.Equipo_external,
            external_equipo: data.external_equipo,
            'external_equipo': data.external_equipo
        }

        const registros = RegistrarOrden(datos).then((info) => {
            console.log("info REGISTRAR ORDEN", info);
            console.log("data REGISTRAR ORDEN", info.data.external_equipo);
            if (info) {
                console.log("session token", info.token);

                MensajeOk("msg: " + info.msg + "\n" + "\n" + "code: " + info.code);
            }
        }, (error) => {
            mensajeError(error.message);
        });

    }

    // Función para filtrar la información según el valor de búsqueda
    const filtrarInfo = () => {
        if (!info) {
            return [];
        }
        if (!search) {
            return info.data;
        }
        return info.data.filter((element) =>
            element.identificacion.toLowerCase().includes(search.toLowerCase())
        );
    }; 

    // Función para manejar cambios en el campo de entrada de texto de búsqueda
    const handleSearch = (event) => {
        setSearch(event.target.value);
    };


    var cantidadDeClaves = Object.keys(errors).length;

    return (
        <div className="">
            <NavBarGerente />
            <div className=""></div>
            <div className="">
                <div className="card text-center">
                    <div className="card-body">
                        <h5 className="card-title alert alert-info">AGENDAR REPARACION</h5>
                        <div className='card'>
                            <div className='card-body'>
                                <button onClick={openModal} type="submit" className='btn btn-success'>Nuevo</button>
                            </div>
                        </div>
                        <div>
                            <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '10px' }}>
                                <div style={{ width: '50%' }}>
                                    <input
                                        type="text"
                                        placeholder="Buscar por Identificación"
                                        value={search}
                                        onChange={handleSearch}
                                        style={{
                                            width: '100%',
                                            height: '40px',
                                            borderRadius: '5px',
                                            padding: '5px 10px',
                                            fontSize: '16px',
                                            border: '1px solid #ccc',
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div style={{ width: "55% ", margin: "30px auto", border: "1px solid black", borderRadius: "5px", height: "600px", overflow: "auto" }}>
                            <table className="table table-striped table-bordered" style={{ width: "100%", margin: "0px auto", border: "1px solid black", borderRadius: "5px" }}>
                                <thead className="thead-dark">
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Apellidos</th>
                                        <th>Marca</th>
                                        <th>Id_Persona</th>
                                        <th>EquipoELectronico</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {filtrarInfo().map((element, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{key + 1}</td>
                                                <td>{element.nombres}</td>
                                                <td>{element.apellidos}</td>
                                                <td>{element.marca}</td>
                                                <td>{element.id_persona}</td>
                                                <td>{element.Equipo_Electronico}</td>
                                                <td>
                                                    <form onSubmit={handleSubmit(onSubmit)}>
                                                        <input
                                                            id="external_equipo"
                                                            type="text" hidden="true"
                                                            className="form-control"
                                                            {...register('external_equipo')}
                                                            value={element.Equipo_external}
                                                            onChange={(e) => {
                                                                const updatedList = [...filtrarInfo()];
                                                                updatedList[key].Equipo_external = e.target.value;
                                                                setFiltrarInfo(updatedList);
                                                            }}
                                                        />
                                                        <button type='submit' className='btn btn-primary'>Generar Orden</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div>
            </div>
        </div>
    );
};

export default AgregarOrden;
