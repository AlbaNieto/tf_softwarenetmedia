import '../css/Bootstrap.css'
import React, { useState } from 'react';
import { Link, Navigate } from 'react-router-dom';
import NavBarGerente from './NavBarGerente';
import { ListarFacturas } from '../hooks/ConexionSw';

const FacturacionView = () => {
    const [llamada, setLlamada] = useState(false);
    const [info, setInfo] = useState(undefined);

    if(!llamada){
        const datos = ListarFacturas().then((data) => {
            setLlamada(true);
            setInfo(data);                    
        }, (error) => {
            console.log(error);
        });
    }
    return (
        <div>
            <NavBarGerente/>
            <div className='container'>
                <h1 className='text-center' style={{ color: "#018f8f" , fontWeight:'bold'}}>LISTA DE FACTURAS</h1>
                <label style={{ color: "#ffffff"}}>Haga click en la reparación que desea facturar</label>
            </div>
            <div className='container'>
                <table className='table'>
                    <thead style={{ color: "#018f8f"}}>
                        <tr>
                            <th>Nro Factura</th>
                            <th>Cliente</th>
                            <th>Dispositivo</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody id='contenidoTabla' style={{ color: "#ffffff" }}>
                        {/*Datos de prueba para comprobar el funcioniemnto de la Factura en PDF*/}
                        {info && info.data && info.data.map((element, key) => {
                            return <tr key={key}>
                                <td>{element.nroFactura}</td>
                                <td>{element.cliente} </td>
                                <td>{element.dispositivo}</td>
                                <td>
                                    <Link to={"/Factura/"+element.external} target="_blank" onClick={<Navigate to='Factura'/>}>Facturar</Link>
                                </td> 
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default FacturacionView;