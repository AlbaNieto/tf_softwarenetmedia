import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import swal from 'sweetalert';
import { ObtenerPersona, RegistrarEquipo, Marcas, RegistrarOrden } from '../hooks/ConexionSw';
import { useNavigate, Link, useParams } from 'react-router-dom';

const mensajeError = (texto) =>
    swal({
        title: 'Error',
        text: texto,
        icon: 'error',
        button: 'Aceptar',
        timer: 10000,
    });
const MensajeOk = (texto) =>
    swal({
        title: 'Ok',
        text: texto,
        icon: 'success',
        button: 'Aceptar',
        html: <a href="//sweetalert2.github.io">links</a>,
        timer: 10000,
    });

function AgregarEquipo() {
    const navegacion = useNavigate();
    const { external } = useParams();
    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();
    const [info, setInfo] = useState(undefined);
    console.log('id ' + external);
    const [llamada, setLlamada] = useState(false);

    const [marcas, setMarcas , setOrden] = useState([]);

    useEffect(() => {
        Marcas().then((data) => {
            if (data) {
                setMarcas(data.data);
                console.log("EQUIPOS DATA", data.data);
            }
        },
            (error) => {
                mensajeError(error.message);
            }
        );
    }, []);

    
    if (!llamada) {
        const datos = ObtenerPersona(external).then(
            (data) => {
                console.log('NOMBRE persona', data.data.Nombres);
                MensajeOk('msg: ' + data.msg + '\n' + '\n' + 'code: ' + data.code);
                if (data) {
                    MensajeOk('msg: ' + data.msg + '\n' + '\n' + 'code: ' + data.code);
                    setValue('nombres', data.data.Nombres);
                    setValue('apellidos', data.data.Apellidos);
                    setValue('direccion', data.data.Direccion);
                    setValue('telefono', data.data.Telefono);
                    setValue('identificacion', data.data.Identificacion);
                    setLlamada(true);
                }
            },
            (error) => {
                mensajeError(error.message);
            }
        );
    }

    const onSubmit = (data) => {
        console.log('ROL----->', data.rol);
        var datos = {
            nombre: data.nombre,
            external_persona: external,
            nombre_marca: data.marca,
            external_equipo: data.external_equipo,
        };
        console.log(":------>>>>", data.external_equipo);

        const registros = RegistrarEquipo(datos).then(
            (info) => {
                console.log("====> external equipo ", datos.external_equipo);
                console.log('info REGISTRO', info);
                if (info) {
                    console.log('session token', info.token);

                    MensajeOk('msg: ' + info.msg + '\n' + '\n' + 'code: ' + info.code);
                }
            },
            (error) => {
                mensajeError(error.message);
            }
        );

        const registrosORden = RegistrarOrden(datos.external_equipo).then(
            (info) => {
                console.log("EXTERNAL EQUIPO ORDEN", info.data.external_equipo);
                console.log("=========== REGISTRO ORDEN");
                console.log('info REGISTRO ORDEENNN', info);
                if (info) {
                    console.log('session token', info.token);

                    MensajeOk('msg: ' + info.msg + '\n' + '\n' + 'code: ' + info.code);
                }
            },
            (error) => {
                mensajeError(error.message);
            }
        );
    };
    var cantidadDeClaves = Object.keys(errors).length;

    return (
        <div style={{ display: "flex", justifyContent: "center", marginTop: "50px" }}>
            <div style={{ width: "50%", backgroundColor: "#f8f9fa", borderRadius: "5px", padding: "20px" }}>
                <h2 style={{ textAlign: "center", color: "#008080" }} className="card-title alert alert-info mb-4">
                    Registrar Equipo
                </h2>
                {errors && cantidadDeClaves > 0 && (
                    <p className="alert alert-danger mb-4">Faltan datos por llenar</p>
                )}
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-group mb-3">
                        <label className="form-label">Nombres</label>
                        <input id="nombres"{...register('nombres', { required: true })} type="text" readOnly={true} className="form-control" />
                    </div>
                    <div className="form-group mb-3">
                        <label className="form-label">Apellidos</label>
                        <input id="apellidos"{...register('apellidos', { required: true })} type="text" readOnly={true} className="form-control" />
                    </div>
                    <div className="form-group mb-3">
                        <label className="form-label">Direccion</label>
                        <input id="direccion"{...register('direccion', { required: true })} type="text" readOnly={true} className="form-control" />
                    </div>
                    <div className="form-group mb-3">
                        <label className="form-label">Telefono</label>
                        <input id="telefono"{...register('telefono', { required: true })} type="text" readOnly={true} className="form-control" />
                    </div>
                    <div className="form-group mb-3">
                        <label className="form-label">Identificacion</label>
                        <input id="identificacion"{...register('identificacion', { required: true })} type="text" readOnly={true} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label style={{width: "9% auto"}} htmlFor="marca">Marca</label>
                        <select className="form-select" id="marca" {...register("marca", { required: true })}>
                            <option value="" disabled>Select Marca</option>
                            {marcas.map((marca) => (
                                <option>{marca.marca}</option>
                            ))}
                        </select>
                    </div>
                    <div className="form-group mb-3">
                        <label className="form-label">Equipo</label>
                        <input id="nombre"{...register('nombre', { required: true })} type="text" className="form-control" />
                    </div>
                    <div className="d-flex justify-content-between">
                        <button type="submit" className="btn btn-primary">Registrar</button>
                        <Link to="/Agendar_Reparacion" id="buttonRegresar" type="submit" className="btn btn-primary" >Regresar</Link>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default AgregarEquipo;
