import '../css/Bootstrap.css';
import React from 'react';
import Modal from 'react-modal';
import NavBarGerente from './NavBarGerente';
import DatePicker from 'react-datepicker';
import { useState } from 'react';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: '70px',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: "#5E5E5E"
    },
};


const ListarAgenda = () => {
    const [fecha, setStartDate] = useState(new Date());
    const [modalIsOpen, setIsOpen] = React.useState(false);

    function openModal() {
        setIsOpen(true);
    }


    function closeModal() {
        setIsOpen(false);
    }

    return (
        <div>
            <NavBarGerente />
            <div className='container'>
                <h1 className='text-center' style={{ fontWeight: "bold" }}>Cambiar Fecha Reparación</h1>
                <div style={{ margin: "30px" }}>
                    <table className="table">
                        <thead>
                            <tr>
                                <th style={{ color: "cyan" }} scope="col">#</th>
                                <th style={{ color: "cyan" }} scope="col">Nombre</th>
                                <th style={{ color: "cyan" }} scope="col">Apellido</th>
                                <th style={{ color: "cyan" }} scope="col">Tipo de Reparación</th>
                                <th style={{ color: "cyan" }} scope="col">Fecha de Reparacion</th>
                                <th style={{ color: "cyan" }} scope="col">Justificación</th>
                            </tr>
                        </thead>
                        <tbody className='thead-dark'>
                            <tr style={{backgroundColor: "white"}}>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>Reparar Touchpad</td>
                                <td>12-05-2022</td>
                                <td>Ninguna</td>
                            </tr>
                            <tr style={{backgroundColor: "white"}}>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>Reparar Pantalla</td>
                                <td>05-12-2022</td>
                                <td>Modelo de pantalla no coincide</td>
                            </tr>
                            <tr style={{backgroundColor: "white"}}>
                                <th scope="row">3</th>
                                <td >Larry</td>
                                <td >Bird</td>
                                <td>Reparar Mainboard</td>
                                <td>05-03-2022</td>
                                <td>Ninguna</td>
                            </tr>
                        </tbody>
                    </table>


                    <div>
                        <button style={{ backgroundColor: "#018f8f" }} onClick={openModal} type="submit" className="btn btn-primary">Cambiar Fecha</button>
                    </div>
                </div>
            </div>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >
                <div style={{ float: "right", margin: "-10px" }}>
                    <svg onClick={closeModal} xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                    </svg>
                </div>
                <h2>Agregar Clientes</h2>
                <form className='agregarCliente' style={{ margin: "20px" }}>
                    <div className='form-group'>
                        <label for="fecha">Ingresar Fecha Limite</label>
                        <DatePicker selected={fecha} dateFormat="dd/MM/yyyy" onChange={(date) => setStartDate(date)} />
                    </div>
                    <div className="form-group">
                        <label for="inputDescripcion">Justificación</label>
                        <textarea className="form-control" id="inputDescripcion" rows="3"></textarea>
                    </div>
                    <button type="submit" className="btn btn-primary">Cambiar</button>
                </form>
            </Modal>
        </div >
    );
};

export default ListarAgenda;