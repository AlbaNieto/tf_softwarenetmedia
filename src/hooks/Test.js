import { useState, useEffect } from "react";
import axios from "axios";
//import { useEffect, useState } from 'react';

//axios para hacer consultar a servicio web,
//antes de que se rendericen todos los componentes

export const Test = (nombre, action = true) => {
  const [info, setInfo] = useState(null);
  const [error, setError] = useState(null);
  useEffect(() => {
    if (action) callApi(nombre);
  }, []);

  const callApi = async (nombre) => {
    try {
      const { data, status, statusText } = await axios.get(
        "http://www.omdbapi.com/?apikey=9698175f&s=" + nombre
      );

      setInfo(data);
      console.log(data);
    } catch (error) {
      setError(error);
      console.log(error);
    }
  };

  //javascript asincrono
  return { info, error, execute: callApi };
};

export default Test;
